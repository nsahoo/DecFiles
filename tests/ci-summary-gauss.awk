BEGIN{
  cnt = 0
}

/FAILED/{
  sub("\\.FAILED", "", $1)
  failures[cnt] = $1
  cnt += 1
}

END{
  if(cnt){
    print "Failures:"
    for(i in failures) {
      print "-", failures[i]
    }
    exit(1)
  } else {
    print "everything OK"
  }
}
