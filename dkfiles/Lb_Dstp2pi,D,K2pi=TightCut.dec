# EventType: 15466420
# 
# Descriptor: [Lambda_b0 -> (D*+ -> (D+ -> K- pi+ pi+) pi0, gamma ) p+ pi- pi-]cc
# 
# NickName: Lb_Dstp2pi,D,K2pi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
## 
# from Configurables import LoKi__GenCutTool 
# from Gauss.Configuration import *
# gen = Generation() 
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' ) 
## 
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay = '^[Lambda_b0 ==> (D*(2010)+ ==> (D+ ==> ^K- ^pi+ ^pi+) (pi0||gamma))  ^p+ ^pi- ^pi-]CC'
# tightCut.Preambulo += [
#    'from GaudiKernel.SystemOfUnits import millimeter,micrometer,MeV,GeV',
#    'inAcc       = in_range ( 0.005 , GTHETA , 0.400 )' ,
#    'inEta       = in_range ( 1.85  , GETA   , 5.050 )' ,
#    'inY         = in_range ( 1.9   , GY     , 4.6   )' ,
#    'goodProton  = ("p+"  == GABSID ) & ( GPT > 0.38 * GeV ) & ( GP  > 8.0 * GeV ) & inAcc & inEta ', 
#    'goodKaon    = ("K+"  == GABSID ) & ( GPT > 0.18 * GeV ) & ( GP  > 2.5 * GeV ) & inAcc & inEta ',
#    'goodPion    = ("pi+" == GABSID ) & ( GPT > 0.18 * GeV ) & ( GP  > 2.5 * GeV ) & inAcc & inEta ',   
#    'goodLambda_b0   =  ( GTIME > 0.05 * millimeter ) &   (GPT > 2.5 * GeV) & inY ',
# ]
# tightCut.Cuts      =    {
#     '[p+]cc'        : 'goodProton'   ,
#     '[K+]cc'        : 'goodKaon'     , 
#     '[pi+]cc'       : 'goodPion'     ,
#     '[Lambda_b0]cc' : 'goodLambda_b0'}
#
# EndInsertPythonCode
# Documentation:   D+ is forced to decay into K- pi+ pi- ( Dalitz model), phase space decay for Lambda_b0
# EndDocumentation
#
# PhysicsWG:   B2OC  
# Tested:      Yes
# CPUTime: 6 min
# Responsible: Aleksandr Berezhnoy
# Email:       Alexander.Berezhnoy@cern.ch
# Date:        20171019
#

Alias	   MyD*-        D*-
Alias	   MyD*+        D*+
ChargeConj MyD*-        MyD*+

Alias       MyD+          D+
Alias       MyD-          D-
ChargeConj  MyD+        MyD-

Decay       Lambda_b0sig
  1.000     MyD*+ p+  pi- pi-  PHSP;
Enddecay
CDecay anti-Lambda_b0sig

Decay MyD*-
0.95	  MyD-   pi0                   VSS;
0.05	  MyD-   gamma                 VSP_PWAVE;
Enddecay
CDecay MyD*+

Decay       MyD+
  1.000     K-   pi+ pi+     D_DALITZ;
Enddecay
CDecay      MyD-

End
