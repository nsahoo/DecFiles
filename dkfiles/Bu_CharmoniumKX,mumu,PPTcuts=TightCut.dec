# EventType: 12445022
#
# Descriptor: [B+ -> (Charmonium -> mu+ mu- X) K+ X]cc
#
# NickName: Bu_CharmoniumKX,mumu,PPTcuts=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool(LoKi__GenCutTool,'TightCut')
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay     = "^( (Beauty & LongLived) --> ( ( J/psi(1S) | psi(2S) ) --> mu+ mu- ...) [K+]cc ... )"
# tightCut.Preambulo = [
#		    "from GaudiKernel.SystemOfUnits import  MeV, GeV",
#		    "inAcc        = in_range ( 0.010 , GTHETA , 0.400 ) &  in_range ( 1.9 , GETA , 5.1 )" ,
#		    "muon_kine    = ( GP > 5 * GeV ) & ( GPT > 1   * GeV   ) " ,
#		    "kaon_kine    = ( GP > 7 * GeV ) & ( GPT > 300 * MeV   ) " ,
#		    "has_mu_plus  = GINTREE ( ( 'mu+' == GID ) & muon_kine & inAcc )" ,
#		    "has_mu_minus = GINTREE ( ( 'mu-' == GID ) & muon_kine & inAcc)" ,
#		    "has_K_plus   = GINTREE ( ( 'K+'  == GID ) & kaon_kine & inAcc)",
#		    "has_K_minus  = GINTREE ( ( 'K-'  == GID ) & kaon_kine & inAcc)" ,
#		    ]
# tightCut.Cuts = {
#    'Beauty' : '(has_mu_plus & has_K_minus) | (has_mu_minus & has_K_plus) | (has_mu_minus & has_K_minus) | (has_mu_plus & has_K_plus)'
#    }
# EndInsertPythonCode
#
# Documentation: Inclusive high momentum Kmu events from B+ -> Charmonium K X decays, only one muon in acceptance
# EndDocumentation
#
# PhysicsWG: B2SL
# Tested: Yes
# CPUTime:< 1min
# Responsible: Svende Braun
# Email: svende.braun@cern.ch
# Date: 20190409
#
Define PKHplus  0.159
Define PKHzero  0.775
Define PKHminus 0.612
Define PKphHplus  1.563
Define PKphHzero  0.0
Define PKphHminus 2.712
#
Alias MyJ/psi J/psi
ChargeConj MyJ/psi MyJ/psi
#
Alias Mychi_c1 chi_c1
ChargeConj Mychi_c1 Mychi_c1
#
Alias Mychi_c0 chi_c0
ChargeConj Mychi_c0 Mychi_c0
#
Alias Mychi_c2 chi_c2
ChargeConj Mychi_c2 Mychi_c2
#
Alias Mypsi(2S) psi(2S)
ChargeConj Mypsi(2S) Mypsi(2S)
#
Alias      MyKst-      K*- 
Alias      MyKst+      K*+ 
ChargeConj MyKst-      MyKst+
#
Alias      MyKst0       K*0 
Alias      Myanti-Kst0  anti-K*0 
ChargeConj MyKst0       Myanti-Kst0
#
Alias      K1(1270)+		K_1+ 
Alias      K1(1270)-  		K_1- 
ChargeConj K1(1270)+       	K1(1270)-
#
Alias      K*0(1430)		K_0*0
Alias      Myanti-K*0(1430)	anti-K_0*0
ChargeConj K*0(1430)		Myanti-K*0(1430)
#
Alias      K*0(1430)+		K_0*+
Alias      K*0(1430)-		K_0*-
ChargeConj K*0(1430)+		K*0(1430)-
#
Decay Mychi_c1
0.3430  MyJ/psi gamma				VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 ;
Enddecay
#
Decay Mychi_c0
0.0140	MyJ/psi gamma				SVP_HELAMP 1.0 0.0 1.0 0.0 ;
Enddecay
#
Decay Mychi_c2
0.1900  MyJ/psi gamma				PHSP ;
Enddecay
#
Decay MyJ/psi
1.00000  mu+ mu-				PHOTOS VLL ;
Enddecay
#
Decay Mypsi(2S)
0.0080  mu+ mu-					PHOTOS VLL;
0.3467  MyJ/psi    pi+        pi-               PHOTOS VVPIPI ;
0.1823  MyJ/psi    pi0        pi0               VVPIPI ;
0.0337  MyJ/psi    eta                          PARTWAVE 0.0 0.0 1.0 0.0 0.0 0.0 ;
0.0979  Mychi_c0   gamma                        PHSP ;
0.0975  Mychi_c1   gamma                        VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 ;
0.0952  Mychi_c2   gamma                        PHSP ;
Enddecay
#
Decay MyKst+
0.3333	K+	pi0				VSS;
Enddecay
CDecay MyKst-
#
Decay MyKst0
0.6667	K+	pi-				VSS;
Enddecay
CDecay Myanti-Kst0
#
Decay K1(1270)+
0.1400  rho0		K+                      VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
0.1867  K*0(1430)	pi+ 			PHSP;
0.0933  K*0(1430)+ 	pi0 			PHSP;
0.1067  MyKst0   	pi+                     VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
0.0533  MyKst+   	pi0                     VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay K1(1270)-
#
Decay K*0(1430)
0.62  K+	pi-				PHSP;
Enddecay
CDecay Myanti-K*0(1430)
#
Decay K*0(1430)+
0.31	K+	pi0				PHSP;
Enddecay
CDecay K*0(1430)-
#
Decay B+sig
0.001010	MyJ/psi		K+		SVS;
0.000810 	MyJ/psi 	K+ 	pi+ pi-	PHSP;
0.001430 	MyJ/psi 	MyKst+ 	    	SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ;
0.001800 	MyJ/psi 	K1(1270)+	SVV_HELAMP 0.5 0.0 1.0 0.0 0.5 0.0 ;
0.000124 	MyJ/psi 	eta 	K+ 	PHSP;
0.000320 	MyJ/psi 	omega 	K+ 	PHSP;
0.000621 	Mypsi(2S) 	K+ 		SVS;
0.000670 	Mypsi(2S) 	MyKst+ 		SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ;
0.000430 	Mypsi(2S)	K+ 	pi+ pi-	PHSP;
0.000149 	Mychi_c0 	K+ 	    	PHSP;
0.000484 	Mychi_c1 	K+		SVS;
0.000300 	Mychi_c1 	MyKst+ 		SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus ;
0.000329 	Mychi_c1 	K+ 	pi0 	PHSP;
0.000374 	Mychi_c1 	K+ 	pi+ pi-	PHSP;
0.000134 	Mychi_c2 	K+ 	pi+ pi-	PHSP;
Enddecay
CDecay B-sig
#
End
#
