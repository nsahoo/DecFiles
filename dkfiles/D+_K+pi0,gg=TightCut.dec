# EventType: 21101420
#
# Descriptor: [D- -> K- ( pi0 -> gamma gamma )]cc 
#
# NickName: D+_K+pi0,gg=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '^[ D+ -> ^K+ ( pi0 -> gamma gamma )]CC'
# tightCut.Cuts      =    {
#     '[K+]cc'  : ' inAcc & piCuts',
#     '[D+]cc'   : 'Dcuts' }
# tightCut.Preambulo += [
#     'inAcc = in_range ( 0.005, GTHETA, 0.400 ) ' , 
#     'piCuts = ( (GPT > 200 * MeV) & ( GP > 600 * MeV))',
#     'Dcuts = (GPT > 1000 * MeV)' ]
# EndInsertPythonCode
#
# Documentation: Inclusive production of D-. D- forced to decay to K- pi0 as phase space, then pi0 to gamma gamma. Used to look at signal when a single gamma converts into e+ e- (Geant) inside LHCb. Generator level cuts on pi and D meson.
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Tom Hadavizadeh
# Email: tom.hadavizadeh@cern.ch
# Date: 20170320
#
Alias      MyPi0        pi0
ChargeConj MyPi0        MyPi0
#
Decay D-sig
  1.00   MyPi0   K-    PHSP;
Enddecay
CDecay D+sig
#
Decay MyPi0
  1.00  gamma gamma     PHSP;
Enddecay
#
End
#
