# EventType: 34124016
#
# Descriptor: K_S0 -> pi+ pi- e+ e-
#
# NickName: KS_pipiee=TightCut,rho
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: K_S0 -> pi+ pi- e+ e-, tight generator cut
#  * KS0 endvertex z in [-1m,1m]
#  * KS0 endvertex radial cut at 30mm
#  * Daughters Theta in [10 mrad, 400 mrad]
# EndDocumentation
#
# CPUTime: < 1 min
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '^(KS0 => ^pi+ ^pi- ^e+ ^e-)'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import meter, millimeter, GeV" ,
#     "GVX = LoKi.GenVertices.PositionX() " ,
#     "GVY = LoKi.GenVertices.PositionY() " ,
#     "GVZ = LoKi.GenVertices.PositionZ() " ,
#     "vx    = GFAEVX ( GVX, 100 * meter )" ,    
#     "vy    = GFAEVX ( GVY, 100 * meter )" ,
#     "rho2  = vx**2 + vy**2 " ,
#     "rhoK  = rho2 < (38 * millimeter )**2 " , 
#     "decay = in_range ( -1 * meter,GFAEVX ( GVZ, 100 * meter ), 0.8 * meter )",
#     "inAcc = in_range (0.010,GTHETA,0.400)"
# ]
# tightCut.Cuts      =    {
#     'KS0'  : ' decay & rhoK',
#     '[e-]cc'  : 'inAcc',
#     '[pi-]cc'  : 'inAcc',
#                         }
# EndInsertPythonCode
#
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Marc-Olivier Bettler, Adrian Casais Vidal
# Email: Marc-Olivier.Bettler@cern.ch, Adrian.Casais.Vidal@cern.ch
# Date: 20181113
#
Decay K_S0sig
  1.000       pi+ pi- e+ e-    PHSP;
Enddecay
#
End

