# EventType:  26166052
#
# Descriptor: [Xi_cc++ -> (D0 -> pi+ K-) p+ K- pi+ pi+]cc
#
# NickName: Xicc++_D0pKpipi,Kpi=GenXicc,DecProdCut,WithMinPT3
#
# Production: GenXicc
#
# Cuts: XiccDaughtersInLHCbAndWithMinPT
#
# CutsOptions: MinXiccPT 3000*MeV
#
# CPUTime: < 1 min
#
# Documentation: Xicc++ decay to D0 p+ K- pi+ pi+ by phase space model, with the D0 decaying to pi+ K-. 
# All daughters of Xicc++ are required to be in the acceptance of LHCb 
# and the Xicc++ PT is required to be larger than 3000 MeV/c. 
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Murdo Traill
# Email: murdo.thomas.traill@cern.ch
# Date: 20170129
#
#Alias	   Xi_cc++sig        Xi_cc++
#Alias	   anti-Xi_cc--sig   anti-Xi_cc--
#ChargeConj Xi_cc++sig		 anti-Xi_cc--sig
#
Alias      MyD0              D0
Alias      Myanti-D0		 anti-D0
ChargeConj MyD0			     Myanti-D0
#
Decay Xi_cc++sig
  1.000    MyD0   p+   K-	  pi+      pi+     PHSP;
Enddecay
CDecay anti-Xi_cc--sig
#
Decay MyD0
  1.000        K-       pi+                   PHSP;
Enddecay
CDecay Myanti-D0
#
End
#
